 <?php
    // Variables de configuración

    // Relacionar contacto con deal
    $dealToContact = function ($dealID, $contactID, $apiKey) {
        $endpoint = 'https://api.hubapi.com/deals/v1/deal/' . $dealID . '/associations/CONTACT?id=' . $contactID . '&hapikey=' . $apiKey;
        $ch = @curl_init($endpoint);

        @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);

        @curl_close($ch);
    };

    // Obtener ID contacto
    $getContactID = function ($email, $apiKey) {
        $endpoint = 'https://api.hubapi.com/contacts/v1/contact/email/' . $email . '/profile?hapikey=' . $apiKey;
        $ch = @curl_init($endpoint);

        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $responseJSON = json_decode($response);

        @curl_close($ch);

        if ($status_code === 200) {
            $contactID = $responseJSON->{'vid'};
            return $contactID;
        }
    };

    // Crear nuevo deal
    $createNewDeal = function ($firstName, $lastName, $finalPercentage, $apiKey, $portalID) {
        $new_deal = array(
            'portalId' => $portalID,
            'properties' => array(
                array(
                    'value' => $firstName . ' ' . $lastName . ' | ' . $finalPercentage,
                    'name' => 'dealname'
                ),
                array(
                    'value' => 'appointmentscheduled',
                    'name' => 'dealstage'
                ),
                array(
                    'value' => 'default',
                    'name' => 'pipeline'
                )
            )
        );
        $deal_json = json_encode($new_deal);
        $endpoint = 'https://api.hubapi.com/deals/v1/deal?hapikey=' . $apiKey;
        $ch = @curl_init($endpoint);

        @curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $deal_json);                                                                  
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                  
        @curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($deal_json))                                                                       
        );

        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result = @curl_exec($ch);
        $responseJSON = json_decode($result);
        $dealID = $responseJSON->{'dealId'};

        @curl_close($ch);

        return $dealID;
    };

    // Enviar form
    $hubspotutk = Cookie::get('hubspotutk');
    $ip_addr = $this->request->ip();
    $hs_context      = array(
        'hutk' => $hubspotutk,
        'ipAddress' => $ip_addr,
        'pageUrl' => 'http://cotiza.snapcar.com.ar/',
        'pageName' => 'SnapCar | Cotizador'
    );
    $hs_context_json = json_encode($hs_context);

    //Varibles de sesion
    $data1 = $this->getSessionData(1);
    $data2 = $this->getSessionData(2);
    $data3 = $this->getSessionData(3);

    $selectedBrand = $data1['brandName'];
    $selectedModel = $data1['modelName'];
    $selectedYear = $data1['year'];
    $isNew = $data1['isNew'];
    $socialUse = $data1['socialUse'];
    $alarm = $data1['alarm'];
    $garage = $data1['garage'];
    $kmMes = $data2['kmMes'];
    $diasMes = $data2['diasMes'];
    $diasPico = $data2['diasPico'];
    $prudente = $data2['prudente'];
    $firstName = $data3['firstName'];
    $lastName = $data3['lastName'];
    $gender = $data3['gender'];
    $birthdate = $data3['birthdate'];
    $email = $data3['email'];
    $phone = $data3['phone'];
    $origen = $data1['origen'];
    $comentario = $data1['comentario'];
    $zip = $data3['postalCode'];

    $isNew = ($isNew == '1' ? 'Si' : 'No');
    $socialUse = ($socialUse == '1' ? 'Si' : 'No');
    $alarm = ($alarm == '1' ? 'Si' : 'No');
    $garage = ($garage == '1' ? 'Si' : 'No');
    $gender = ($gender == 'M' ? 'Masculino' : 'Femenino');

    $str_post = "firstname=" . urlencode($firstName)
        . "&lastname=" . urlencode($lastName)    
        . "&sexo=" . urlencode($gender)
        . "&nacimiento=" . urlencode($birthdate)
        . "&email=" . urlencode($email)
        . "&phone=" . urlencode($phone)   
        . "&origen=" . urlencode($origen)
        . "&comentario=" . urlencode($comentario)
        . "&cp=" . urlencode($zip)
        . "&marca=" . urlencode($selectedBrand)
        . "&modelo=" . urlencode($selectedModel)
        . "&year=" . urlencode($selectedYear)
        . "&cero_km=" . urlencode($isNew)
        . "&particular=" . urlencode($socialUse)
        . "&alarma=" . urlencode($alarm)
        . "&garage=" . urlencode($garage)
        . "&km_mes=" . urlencode($kmMes)
        . "&dias_mes=" . urlencode($diasMes)
        . "&dias_pico=" . urlencode($diasPico)
        . "&prudente=" . urlencode($prudente)
        . "&porcentaje=" . urlencode($finalPercentage)
        . "&hs_context=" . urlencode($hs_context_json);

    // Datos Test
    $endpoint = 'https://forms.hubspot.com/uploads/form/v2/' . $portalID . '/' . $formGUID;

    $ch = @curl_init();
    @curl_setopt($ch, CURLOPT_POST, true);
    @curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
    @curl_setopt($ch, CURLOPT_URL, $endpoint);
    @curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/x-www-form-urlencoded'
    ));
    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response    = @curl_exec($ch);
    $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
    @curl_close($ch);

    if ($status_code === 204) {
        sleep(5);
        $contact = $getContactID($email, $apiKey);
        $deal = $createNewDeal($firstName, $lastName, $finalPercentage, $apiKey, $portalID);
        $dealToContact($deal, $contact, $apiKey);
    }
?>
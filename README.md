## Variables de Configuración ##

### Producción
```php
$apiKey = '49dfd52a-a898-424f-8f38-fe2eaaab5fa6';
$portalID = '2800177';
$formGUID = '898b2475-51a5-4633-99d1-82df1f61cf21';
```

### Test
```php
$apiKey = '81743c6f-696f-4306-a497-89f6c771f0d2';
$portalID = '2866523';
$formGUID = '005fb709-7164-4462-8f8b-6fd8a3356d25';
```

## Stage IDs

**Step 3**

c5f4684e-3ef0-4eb9-b94e-213a1bb4a88a

**Step 4**

238a02ab-5989-41e7-8797-d674c9f4cdcf

**Step 5**

d4512d23-836c-4d40-8c33-f0e90fac44f6

**Step 6**

a3d45589-ef5b-4f21-b347-67a6a1df5d61

## Deals API

Desde acá se debería ir actualizando la propiedad dealstage con los stage IDs correspondientes.

Documentación: http://developers.hubspot.com/docs/methods/deals/update_deal

## Capturar campos

Respetar los nombres de los campos al enviarlos por URL ya que corresponden a los nombres de los campos dentro del formulario de HubSpot.

```
provincia
localidad
tipo_documento
nro_documento
patente
nro_motor
nro_chasis
address
nro_calle
piso
departamento
phone_2
```